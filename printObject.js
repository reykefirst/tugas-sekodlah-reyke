function printData(data) {
    for(let i=0; i<data.length; i++) {
      console.log(`${data[i].id}.`,`Nama: ${data[i].name}`,`Umur: ${data[i].age}`, `Kota: ${data[i].city}`);
    }
  }
  
  // Contoh penggunaan
  const data = [
    { id: 1, name: 'Udin', age: 25, city: 'Jakarta' },
    { id: 2, name: 'ujang', age: 30, city: 'Bandung' },
    { id: 3, name: 'Asep', age: 35, city: 'Surabaya' },
  ];
  
  printData(data);
  
  