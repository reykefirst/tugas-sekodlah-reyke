function generateRandomString(length) {
    const karakter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-+=[]{}|;:,.<>/?";
    let hasil = "";
    for (let i = 0; i < length; i++) {
      const randomIndex = Math.floor(Math.random() * karakter.length);
      hasil += karakter[randomIndex];
    }
    return hasil;
  }
  
  // Contoh penggunaan
  console.log(generateRandomString(10));