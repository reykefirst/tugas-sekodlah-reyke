const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function repeatString(inputString, maxNum) {
  let result = '';
  for (let i = 1; i <= maxNum; i++) {
    result += inputString + '';
  }
  return result;
}

function countA(inputString) {
  let count = 0;
  for (let i = 0; i < inputString.length; i++) {
    if (inputString[i].toLowerCase() === 'a') {
      count++;
    }
  }
  return count;
}

rl.question('Masukkan kata yang ingin diulang: ', (input) => {
  rl.question('Masukkan jumlah maksimal pengulangan: ', (max) => {
    const repeatedString = repeatString(input, max);
    console.log('Hasil : ');
    console.log(`${input} * ${max} = ${repeatedString}`);
    console.log(`Jumlah huruf 'a' pada string diatas : ${countA(repeatedString)}`);
    rl.close();
  });
});
