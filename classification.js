const people = [
    { name: "udin", sex: "male", age: 10, marriage: "single", status: "student" },
    { name: "ujang", sex: "male", age: 11, marriage: "double", status: "employee" },
    { name: "asep", sex: "male", age: 12, marriage: "single", status: "employee" },
    { name: "icih", sex: "female", age: 19, marriage: "single", status: "student" },
    { name: "eneng", sex: "female", age: 25, marriage: "double", status: "employee" }
];

const classification = {
    sex: {
        male: [],
        female: []
    },
    age: {
        under20: [],
        older: []
    },
    marriage: {
        single: [],
        double: []
    },
    status: {
        student: [],
        employee: []
    }
};

for (let person of people) {
    // Pengelompokan berdasarkan jenis kelamin
    if (person.sex === "male") {
        classification.sex.male.push(person.name);
    } else {
        classification.sex.female.push(person.name);
    }

    // Pengelompokan berdasarkan usia
    if (person.age < 20) {
        classification.age.under20.push(person.name);
    } else {
        classification.age.older.push(person.name);
    }

    // Pengelompokan berdasarkan status perkawinan
    if (person.marriage === "single") {
        classification.marriage.single.push(person.name);
    } else {
        classification.marriage.double.push(person.name);
    }

    // Pengelompokan berdasarkan status pekerjaan
    if (person.status === "student") {
        classification.status.student.push(person.name);
    } else {
        classification.status.employee.push(person.name);
    }
}

console.log(classification);
